﻿using System.Collections.Generic;

namespace Day3
{
    public class Map
    {
        private readonly Dictionary<(int x, int y), int> _map = new Dictionary<(int x, int y), int>();

        public int VisitedHouses => _map.Count;

        public void VisitHouse((int x, int y) position)
        {
            if (_map.ContainsKey(position))
                _map[position] += 1;
            else
                _map[position] = 1;
        }
    }
}
