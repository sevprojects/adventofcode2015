﻿using System;
using System.IO;

namespace Day3
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = File.ReadAllText("input.txt");
            var map = new Map();
            var santas = new SantaManager(2);

            foreach (var direction in input)
            {
                var santa = santas.GetNextSanta();

                MoveSanta(direction, santa);
                map.VisitHouse(santa.Position);
            }

            Console.WriteLine($"Number of houses with at least one present: {map.VisitedHouses}");
        }

        private static void MoveSanta(char direction, Santa santa)
        {
            switch (direction)
            {
                case '^': santa.MoveNorth(); break;
                case 'v': santa.MoveSouth(); break;
                case '>': santa.MoveEast(); break;
                case '<': santa.MoveWest(); break;
                default: throw new ArgumentException("Invalid direction: " + direction);
            }
        }
    }
}
