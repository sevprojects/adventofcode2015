﻿using System;

namespace Day3
{
    public class SantaManager
    {
        private readonly Santa[] _santas;
        private int _currentIndex;

        public SantaManager(int numberOfSantas)
        {
            if (numberOfSantas <= 0)
                throw new ArgumentException("Required at least one Santa");

            _currentIndex = -1;
            _santas = new Santa[numberOfSantas];

            for (var i = 0; i < numberOfSantas; i++)
                _santas[i] = new Santa();
        }

        public Santa GetNextSanta()
        {
            _currentIndex++;

            if (_currentIndex >= _santas.Length)
                _currentIndex = 0;

            return _santas[_currentIndex];
        }
    }
}
