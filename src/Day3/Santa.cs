﻿namespace Day3
{
    public class Santa
    {
        private int _x = 0;
        private int _y = 0;

        public (int x, int y) Position => (_x, _y);

        public Santa()
        {
        }

        public void MoveNorth() => _y++;
        public void MoveSouth() => _y--;
        public void MoveEast() => _x++;
        public void MoveWest() => _x--;
    }
}
