﻿using System;
using System.IO;

namespace Day6
{
    public class Program
    {
        public static bool IsPartOne = false;

        public static void Main(string[] args)
        {
            var inputs = File.ReadAllLines("input.txt");
            var parser = new InputParser(inputs);
            var map = new Map();

            foreach (var input in parser.ReadInputs())
            {
                map.Execute(input.Command, input.FromPosition, input.ToPosition);
            }

            Console.WriteLine($"{map.TurnedOnLights} lights are lit");
            Console.WriteLine($"And total brightness is {map.TotalBrightness}");
        }
    }
}
