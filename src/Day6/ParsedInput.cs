﻿using Day6.Commands;

namespace Day6
{
    public class ParsedInput
    {
        public ICommand Command { get; set; }
        public (int X, int Y) FromPosition { get; set; }
        public (int X, int Y) ToPosition { get; set; }
    }
}
