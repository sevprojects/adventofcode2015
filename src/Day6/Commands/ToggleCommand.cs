﻿namespace Day6.Commands
{
    public class ToggleCommand : ICommand
    {
        public int Execute(int input)
        {
            if (Program.IsPartOne)
                return ExecutePartOne(input);
            else
                return ExecurePartTwo(input);
        }

        private int ExecutePartOne(int input)
        {
            if (input == 0)
                return 1;
            else
                return 0;
        }

        private int ExecurePartTwo(int input)
        {
            return input + 2;
        }
    }
}
