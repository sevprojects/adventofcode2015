﻿namespace Day6.Commands
{
    public interface ICommand
    {
        int Execute(int input);
    }
}
