﻿namespace Day6.Commands
{
    public class OnCommand : ICommand
    {
        public int Execute(int input)
        {
            if (Program.IsPartOne)
                return ExecutePartOne(input);
            else
                return ExecurePartTwo(input);
        }

        private int ExecutePartOne(int input)
        {
            return 1;
        }

        private int ExecurePartTwo(int input)
        {
            return ++input;
        }
    }
}
