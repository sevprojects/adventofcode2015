﻿namespace Day6.Commands
{
    public class OffCommand : ICommand
    {
        public int Execute(int input)
        {
            if (Program.IsPartOne)
                return ExecutePartOne(input);
            else
                return ExecurePartTwo(input);
        }

        private int ExecutePartOne(int input)
        {
            return 0;
        }

        private int ExecurePartTwo(int input)
        {
            if (input == 0)
                return 0;

            return --input;
        }
    }
}
