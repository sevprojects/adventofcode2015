﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day6
{
    public class InputParser
    {
        private readonly string[] _input;
        private readonly CommandFactory _commandFactory;
        private readonly Regex _positions = new Regex(@"(\d+\,\d+)");

        public InputParser(string[] input)
        {
            _input = input;
            _commandFactory = new CommandFactory();
        }

        public IEnumerable<ParsedInput> ReadInputs()
        {
            foreach (var input in _input)
            {
                yield return Parse(input);
            }
        }

        private ParsedInput Parse(string input)
        {
            var matches = _positions.Matches(input);
            var from = matches[0].Value.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            var to = matches[1].Value.Split(',').Select(x => Convert.ToInt32(x)).ToList();

            return new ParsedInput
            {
                Command = _commandFactory.GetCommand(input),
                FromPosition = (from[0], from[1]),
                ToPosition = (to[0], to[1])
            };
        }
    }
}
