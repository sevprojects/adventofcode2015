﻿using Day6.Commands;
using System.Collections.Generic;
using System.Linq;

namespace Day6
{
    public class Map
    {
        private const int InitialStatus = 0;

        private readonly Dictionary<(int x, int y), int> _map = new Dictionary<(int x, int y), int>();

        public int TurnedOnLights => _map.Values.Count(x => x > 0);

        public int TotalBrightness => _map.Values.Sum();

        public void Execute(ICommand command, (int X, int Y) from, (int X, int Y) to)
        {
            for (int x = from.X; x <= to.X; x++)
            {
                for (int y = from.Y; y <= to.Y; y++)
                {
                    if (_map.ContainsKey((x, y)))
                        _map[(x, y)] = command.Execute(_map[(x, y)]);
                    else
                        _map[(x, y)] = command.Execute(InitialStatus);
                }
            }
        }
    }
}
