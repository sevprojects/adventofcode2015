﻿using Day6.Commands;
using System;

namespace Day6
{
    public class CommandFactory
    {
        private const string Toggle = "toggle";
        private const string TurnOn = "turn on";
        private const string TurnOff = "turn off";

        private static readonly ICommand _onCommand = new OnCommand();
        private static readonly ICommand _offCommand = new OffCommand();
        private static readonly ICommand _toggleCommand = new ToggleCommand();

        public ICommand GetCommand(string input)
        {
            if (input.StartsWith(Toggle))
                return _toggleCommand;

            if (input.StartsWith(TurnOn))
                return _onCommand;

            if (input.StartsWith(TurnOff))
                return _offCommand;

            throw new ArgumentException($"Invalid command input: {input}");
        }
    }
}
