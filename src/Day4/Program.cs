﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Day4
{
    public class Program
    {
        private const string _input = "iwrupvqb";
        private static readonly MD5 _md5 = MD5.Create();

        public static void Main(string[] args)
        {
            var number = 1;

            while (!IsLowestPositiveNumber(number))
            {
                number++;
            }

            Console.WriteLine($"Lowest positive number is {number}");
        }

        private static bool IsLowestPositiveNumber(int number)
        {
            var hash = _md5.ComputeHash(Encoding.UTF8.GetBytes(_input + number));
            var leadingString = string.Empty;

            for (var i = 0; i < 3; i++)
            {
                leadingString += hash[i].ToString("X2");
            }

            //return leadingString.StartsWith("00000"); // part 1
            return leadingString.StartsWith("000000"); // part 2
        }
    }
}
