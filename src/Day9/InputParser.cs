﻿using Day9.Models;
using System.Collections.Generic;

namespace Day9
{
    public class InputParser
    {
        private readonly string[] _input;

        public InputParser(string[] input)
        {
            _input = input;
        }

        public IEnumerable<Distance> ReadInput()
        {
            foreach (var input in _input)
            {
                var parts = input.Split(' ');
                var length = int.Parse(parts[4]);

                yield return new Distance
                {
                    Length = length,
                    Start = parts[0],
                    Stop = parts[2]
                };

                yield return new Distance
                {
                    Length = length,
                    Start = parts[2],
                    Stop = parts[0]
                };
            }
        }
    }
}
