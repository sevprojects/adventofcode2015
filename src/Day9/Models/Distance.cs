﻿namespace Day9.Models
{
    public class Distance
    {
        public string Start { get; set; }
        public string Stop { get; set; }
        public int Length { get; set; }
    }
}
