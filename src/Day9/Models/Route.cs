﻿using System.Collections.Generic;

namespace Day9.Models
{
    public class Route
    {
        public IEnumerable<string> Cities { get; set; }
        public int TotalLength { get; set; }
    }
}
