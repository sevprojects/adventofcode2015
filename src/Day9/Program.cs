﻿using Combinatorics.Collections;
using Day9.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day9
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputs = File.ReadAllLines("input.txt");
            var parser = new InputParser(inputs);

            var distances = parser.ReadInput().ToList();
            var cities = distances.Select(x => x.Start).Distinct().ToList();
            var permutations = new Permutations<string>(cities, GenerateOption.WithoutRepetition);

            var routes = permutations.Select(x => BuildRoute(x, distances));

            Console.WriteLine($"The shortest route is {routes.Min(x => x.TotalLength)}");
            Console.WriteLine($"The longest route is {routes.Max(x => x.TotalLength)}");
        }

        private static Route BuildRoute(IList<string> cities, List<Distance> distances)
        {
            var route = new Route
            {
                Cities = cities
            };

            for (var i = 1; i < cities.Count; i++)
            {
                route.TotalLength += distances
                    .Find(x => 
                        x.Start.Equals(cities[i - 1]) &&
                        x.Stop.Equals(cities[i]))
                    .Length;
            }

            return route;
        }
    }
}
