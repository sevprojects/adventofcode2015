﻿using System;
using System.IO;

namespace Day1
{
    /// <summary>
    /// https://adventofcode.com/2015/day/1
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = File.ReadAllText("input.txt");
            var floor = 0;
            var basementPosition = 0;

            for (var i = 0; i < input.Length; i++)
            {
                var direction = input[i];

                switch (direction)
                {
                    case '(': floor++; break;
                    case ')': floor--; break;
                    default: throw new ArgumentException("Invalid direction: " + direction);
                }

                if (basementPosition == 0 && floor == -1)
                    basementPosition = i + 1;
            }

            Console.WriteLine($"Provided instructions will take Santa to floor {floor}");
            Console.WriteLine($"And he will first enter basement with instruction number {basementPosition}");
        }
    }
}
