﻿using System;
using System.Text;

namespace Day10
{
    public class Program
    {
        private static readonly StringBuilder _strBuilder = new StringBuilder();

        public static void Main(string[] args)
        {
            var input = "3113322113";
            var repetitions = 50;

            for (var i = 0; i < repetitions; i++)
                input = LookAndSay(input);

            Console.WriteLine($"Length: {input.Length}");
        }

        private static string LookAndSay(string input)
        {
            if (input.Length == 1)
                return "1" + input;

            var span = input.AsSpan();
            _strBuilder.Clear();

            while (span.Length > 0)
            {
                var (occur, val) = Occurences(span);
                _strBuilder.Append(occur).Append(val);
                span = span.Slice(occur);
            }

            return _strBuilder.ToString();
        }

        private static (int occurences, char val) Occurences(ReadOnlySpan<char> str)
        {
            var occurrences = 1;
            var firstChar = str[0];

            for (var i = 1; i < str.Length; i++)
            {
                if (firstChar == str[i])
                    occurrences++;
                else
                    break;
            }

            return (occurrences, firstChar);
        }
    }
}
