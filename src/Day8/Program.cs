﻿using System;
using System.IO;

namespace Day8
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var totalOfCode = 0;
            var totalEncoded = 0;
            var totalInMemory = 0;

            var inputs = File.ReadAllLines("input.txt");

            foreach (var input in inputs)
            {
                var (code, memory) = CalculatePartOne(input);
                totalEncoded += CalculatePartTwo(input);
                totalOfCode += code;
                totalInMemory += memory;
            }

            Console.WriteLine($"Part one result: {totalOfCode - totalInMemory}");
            Console.WriteLine($"Part two result: {totalEncoded - totalOfCode}");
        }

        private static (int code, int memory) CalculatePartOne(string input)
        {
            var total = input.Length;
            var word = input.Trim('"');

            var totalOfCode = total;

            word = word
                .Replace("\\\\", "?")
                .Replace("\\\"", "?");
            total = word.Length;

            var occur = Occurences(word, "\\x");
            total -= occur * 3;

            var totalInMemory = total;

            return (totalOfCode, totalInMemory);
        }

        private static int CalculatePartTwo(string input)
        {
            var word = input
                .Replace("\\", "\\\\")
                .Replace("\"", "\\\"");

            return word.Length + 2;
        }

        private static int Occurences(string str, string val)
        {
            var occurrences = 0;
            var startingIndex = 0;

            while ((startingIndex = str.IndexOf(val, startingIndex)) >= 0)
            {
                occurrences++;
                startingIndex += val.Length;
            }

            return occurrences;
        }
    }
}
