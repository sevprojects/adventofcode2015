﻿using System;
using System.IO;
using System.Linq;

namespace Day5
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputs = File.ReadAllLines("input.txt");
            //var determiner = new PartOneDeterminer();
            var determiner = new PartTwoDeterminer();

            var numberOfNiceStrings = inputs.Count(determiner.IsNice);

            Console.WriteLine($"Number of nice strings: {numberOfNiceStrings}");
        }
    }
}
