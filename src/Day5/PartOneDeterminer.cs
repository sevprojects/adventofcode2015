﻿using System;
using System.Linq;

namespace Day5
{
    public class PartOneDeterminer : INiceStringDeterminer
    {
        private readonly char[] _vowels = new char[] { 'a', 'e', 'i', 'o', 'u' };
        private readonly string[] _forbidden = new string[] { "ab", "cd", "pq", "xy" };

        public bool IsNice(string input)
        {
            var vowelsCount = 0;
            var doublesCount = 0;
            var containsForbidden = false;

            for (int i = 0; i < input.Length; i++)
            {
                var c = input[i];
                var nextC = i + 1 == input.Length
                    ? (char?)null
                    : input[i + 1];

                if (IsVowel(c)) vowelsCount++;
                if (IsDouble(c, nextC)) doublesCount++;
                containsForbidden |= IsForbidden(c, nextC);
            }

            return !containsForbidden
                && doublesCount > 0
                && vowelsCount >= 3;
        }

        private bool IsVowel(char input)
        {
            return _vowels.Contains(input);
        }

        private bool IsDouble(char input, char? nextInput)
        {
            return input == nextInput;
        }

        private bool IsForbidden(char c, char? nextC)
        {
            return _forbidden.Any(x => x[0] == c && x[1] == nextC);
        }
    }
}
