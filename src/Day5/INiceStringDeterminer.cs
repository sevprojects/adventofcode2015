﻿namespace Day5
{
    public interface INiceStringDeterminer
    {
        bool IsNice(string input);
    }
}
