﻿using System.Text.RegularExpressions;

namespace Day5
{
    public class PartTwoDeterminer : INiceStringDeterminer
    {
        private readonly Regex _twiceTwo = new Regex(@"(..).*\1");
        private readonly Regex _twiceOne = new Regex(@"(.).\1");

        public bool IsNice(string input)
        {
            return _twiceOne.IsMatch(input)
                && _twiceTwo.IsMatch(input);
        }
    }
}
