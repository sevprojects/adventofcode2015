﻿using CsvHelper.Configuration.Attributes;

namespace Day2
{
    public class Box
    {
        [Index(0)]
        public int Length { get; set; }

        [Index(1)]
        public int Width { get; set; }

        [Index(2)]
        public int Height { get; set; }
    }
}
