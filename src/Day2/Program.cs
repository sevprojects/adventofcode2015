﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var squareFeetsOfPaper = 0;
            var feetsOfRibbon = 0;

            using var sr = new StreamReader("input.txt");
            using var reader = new CsvReader(sr);

            reader.Configuration.HasHeaderRecord = false;
            reader.Configuration.Delimiter = "x";

            var boxes = reader.GetRecords<Box>();

            foreach (var box in boxes)
            {
                squareFeetsOfPaper += CalculateRequiredPaper(box);
                feetsOfRibbon += CalculateRequiredRibbon(box);
            }

            Console.WriteLine($"Elves would need total of {squareFeetsOfPaper} square feet of wrapping paper");
            Console.WriteLine($"And total of {feetsOfRibbon} feet of ribbon");
        }

        private static int CalculateRequiredPaper(Box box)
        {
            var a = box.Length * box.Width;
            var b = box.Width * box.Height;
            var c = box.Height * box.Length;
            var slack = MinValue(a, b, c);

            return (2 * a) + (2 * b) + (2 * c) + slack;
        }

        private static int CalculateRequiredRibbon(Box box)
        {
            var bow = 1;
            var array = new List<int> { box.Height, box.Length, box.Width };

            array.Sort();

            foreach (var number in array)
                bow *= number;

            var smallestPerimieter = (2 * array[0]) + (2 * array[1]);

            return bow + smallestPerimieter;
        }

        private static int MinValue(params int[] values)
        {
            return Enumerable.Min(values);
        }
    }
}
