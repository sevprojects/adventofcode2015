﻿using System.Collections.Generic;

namespace Day7.Models
{
    public class ParseResult
    {
        public Operation Operation { get; set; }
        public Dictionary<string, int> Values { get; set; } = new Dictionary<string, int>();
    }
}
