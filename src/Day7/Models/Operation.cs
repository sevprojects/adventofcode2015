﻿using Day7.Gates;

namespace Day7.Models
{
    public class Operation
    {
        public IGate Gate { get; set; }
        public string FirstInputName { get; set; }
        public string SecondInputName { get; set; }
        public string OutputName { get; set; }
    }
}
