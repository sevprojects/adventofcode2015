﻿using Day7.Gates;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day7
{
    public class GateFactory
    {
        private readonly Dictionary<string, IGate> _cache = new Dictionary<string, IGate>
        {
            ["OR"] = new OrGate(),
            ["AND"] = new AndGate(),
            ["NOT"] = new NotGate(),
            ["LSHIFT"] = new LeftShiftGate(),
            ["RSHIFT"] = new RightShiftGate(),
            ["PROXY"] = new ProxyGate()
        };

        public IGate CreateGate(string operation)
        {
            var gate = _cache
                .Where(x => operation.Equals(x.Key))
                .FirstOrDefault()
                .Value;

            if (gate == null)
                throw new ArgumentException($"Invalid operation: {operation}");

            return gate;
        }
    }
}
