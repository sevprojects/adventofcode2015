﻿namespace Day7.Gates
{
    public class AndGate : IGate
    {
        public int Execute(int a, int b)
        {
            return a & b;
        }
    }
}
