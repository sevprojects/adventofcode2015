﻿namespace Day7.Gates
{
    public class NotGate : IGate
    {
        public int Execute(int a, int b)
        {
            return ~a;
        }
    }
}
