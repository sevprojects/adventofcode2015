﻿namespace Day7.Gates
{
    public class OrGate : IGate
    {
        public int Execute(int a, int b)
        {
            return a | b;
        }
    }
}
