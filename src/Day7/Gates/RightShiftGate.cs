﻿namespace Day7.Gates
{
    public class RightShiftGate : IGate
    {
        public int Execute(int a, int b)
        {
            return a >> b;
        }
    }
}
