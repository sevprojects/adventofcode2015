﻿namespace Day7.Gates
{
    public interface IGate
    {
        int Execute(int a, int b);
    }
}
