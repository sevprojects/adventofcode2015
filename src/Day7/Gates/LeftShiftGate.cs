﻿namespace Day7.Gates
{
    public class LeftShiftGate : IGate
    {
        public int Execute(int a, int b)
        {
            return a << b;
        }
    }
}
