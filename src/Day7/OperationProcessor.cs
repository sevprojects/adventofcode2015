﻿using Day7.Gates;
using Day7.Models;
using System;
using System.Collections.Generic;

namespace Day7
{
    public class OperationProcessor
    {
        private readonly Dictionary<string, int> _results = new Dictionary<string, int>();
        private readonly List<Operation> _operations = new List<Operation>();

        public OperationProcessor(IEnumerable<ParseResult> parseResults)
        {
            Initialize(parseResults);
        }

        private void Initialize(IEnumerable<ParseResult> parseResults)
        {
            foreach (var result in parseResults)
            {
                if (result.Operation != null)
                    _operations.Add(result.Operation);

                foreach (var value in result.Values)
                    _results.Add(value.Key, value.Value);
            }
        }

        public int ProcessResultFor(string name)
        {
            if (_results.TryGetValue(name, out var result))
                return result;

            var operation = _operations.Find(x => x.OutputName.Equals(name))
                ?? throw new ArgumentException($"Can not process result for {name}. Insufficient data.");

            if (!_results.TryGetValue(operation.FirstInputName, out var firstInput))
                firstInput = ProcessResultFor(operation.FirstInputName);

            if (string.IsNullOrEmpty(operation.SecondInputName))
                return ExecuteOperation(operation.Gate, firstInput, 0, operation.OutputName);

            if (!_results.TryGetValue(operation.SecondInputName, out var secondInput))
                secondInput = ProcessResultFor(operation.SecondInputName);

            return ExecuteOperation(operation.Gate, firstInput, secondInput, operation.OutputName);
        }

        private int ExecuteOperation(IGate gate, int firstInput, int secondInput, string outputName)
        {
            var result = gate.Execute(firstInput, secondInput);
            _results.Add(outputName, result);

            return result;
        }
    }
}
