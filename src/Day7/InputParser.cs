﻿using Day7.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Day7
{
    public class InputParser
    {
        private readonly string[] _input;
        private readonly GateFactory _gateFactory;

        private readonly Regex _regex = new Regex(@"(?:(?<st>[a-z]*[0-9]*)\s)?(?:(?<gate>\w+)\s)?(?<nd>\w+)\s->\s(?<out>\w+)");

        public InputParser(string[] input)
        {
            _input = input;
            _gateFactory = new GateFactory();
        }

        public IEnumerable<ParseResult> ParseInputs()
        {
            foreach (var input in _input)
            {
                yield return Parse(input);
            }
        }

        private ParseResult Parse(string input)
        {
            var result = new ParseResult();
            var match = _regex.Match(input);
            var secondInput = match.Groups["nd"].Value;
            var firstInput = match.Groups["st"].Value;
            var output = match.Groups["out"].Value;
            var gate = match.Groups["gate"].Value;

            if (string.IsNullOrWhiteSpace(gate))
            {
                if (int.TryParse(secondInput, out var ndValue))
                {
                    result.Values.Add(output, ndValue);
                }
                else
                {
                    result.Operation = new Operation
                    {
                        FirstInputName = secondInput,
                        OutputName = output,
                        Gate = _gateFactory.CreateGate("PROXY")
                    };
                }

                return result;
            }

            if (!string.IsNullOrWhiteSpace(secondInput) && int.TryParse(secondInput, out var secondValue))
            {
                secondInput = Guid.NewGuid().ToString();
                result.Values.Add(secondInput, secondValue);
            }

            if (!string.IsNullOrWhiteSpace(firstInput) && int.TryParse(firstInput, out var firstValue))
            {
                firstInput = Guid.NewGuid().ToString();
                result.Values.Add(firstInput, firstValue);
            }

            if (string.IsNullOrWhiteSpace(firstInput))
            {
                firstInput = secondInput;
                secondInput = null;
            }

            result.Operation = new Operation
            {
                FirstInputName = firstInput,
                SecondInputName = secondInput,
                OutputName = output,
                Gate = _gateFactory.CreateGate(gate)
            };

            return result;
        }
    }
}
