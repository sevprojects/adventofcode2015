﻿using Day7.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day7
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputs = File.ReadAllLines("input.txt");
            var parser = new InputParser(inputs);
            var parsed = parser.ParseInputs().ToList();

            var signalA = PartA(parsed);
            PartB(parsed, signalA);
        }

        private static int PartA(List<ParseResult> parsed)
        {
            var processor = new OperationProcessor(parsed);
            var signalA = processor.ProcessResultFor("a");

            Console.WriteLine($"Wire a is provided with signal {signalA}");

            return signalA;
        }

        private static void PartB(List<ParseResult> parsed, int signalA)
        {
            var resultB = parsed.Find(x => x.Values.Any(v => v.Key.Equals("b")));
            resultB.Values["b"] = signalA;

            var processor = new OperationProcessor(parsed);
            signalA = processor.ProcessResultFor("a");

            Console.WriteLine($"And new signal for wire a is {signalA}");
        }
    }
}
